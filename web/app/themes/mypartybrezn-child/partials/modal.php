<a id="announcement" href="#animated-modal"></a>

<div class="animated-modal" id="animated-modal">
    <div class="close-animated-modal">
        <img class="close-btn" src="../../app/themes/mypartybrezn-child/assets/icons/closebt.svg">
    </div>

    <div class="modal-content">
        <div class="container">
            <h1>Brezn und Gebäcke für jeden Geschmack</h1>    
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="text-box">
                        <h2 class="step-headline">Riesenschaumrolle</h2>
                        <a href="/riesenbrezn-riesengebaeck/">
                            <div class="overlay-wrap">
                                <img src="/app/uploads/2019/08/mypartybrezn_riesenschaumrolle.jpg"
                                    class="modal-img">
                                <img src="../app/themes/mypartybrezn-child/assets/images/overlay_new.png"
                                    class="overlay-new" />
                            </div>
                        </a>
                        <p class="text-information">Der Hit auf jeder Feier – mit der neuen Riesenschaumrolle aus der maxi.backstube spielen Sie auf jeder Feier die Hauptrolle. Mit ca. 50 cm Länge und ca. 20 cm Durchmesser ist sie echt riiiiiesengroß. Selbstgemachter Blätterteig und die reichliche Fülle mit hausgemachtem Eiweißschaum sorgen für runden Genuss.</p>
                    </div>
                    <div class="text-center mt-3"> <a class="mypartybrezn-btn" href="/riesenbrezn-riesengebaeck/">Zum
                            Sortiment</a></div>
                </div>
            </div>
        </div>
    </div>
</div>


<a id="announcement-store" href="#animated-modal-small"></a>

<div class="container animated-modal" id="animated-modal-small">
    <div class="close-animated-modal-small">
        <img class="close-btn" src="../../app/themes/mypartybrezn-child/assets/icons/closebt.svg">
    </div>

    <div class="modal-content">
        <div class="container">
            <h1>Neueröffnung in Ried</h1>
            <div class="row">
                <div class="col-12 col-md-6 mx-auto">
                    <div class="text-box">
                        <a href="/riesenbrezn-riesengebaeck/">
                            <div class="overlay-wrap">
                                <img src="/app/uploads/2019/05/mypartybrezn_vegetarisch.jpg"
                                    class="modal-img">
                            </div>
                        </a>
                        <p class="text-information">Für den Zeitraum vom 25.5. bis 28.5. kann im Maximarkt Ried aufgrund
                            der Übersiedlung in den neuen Standort keine Bestellung aufgegeben werden. Ab 29.5. steht
                            für Sie das gesamte MYPARTYBREZN-Sortiment wieder zur Verfügung!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>