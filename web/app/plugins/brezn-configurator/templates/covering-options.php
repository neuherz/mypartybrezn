<?php
$ingredients = apply_filters('get_ingredients', 10, 3);
?>

<div class="configurator-step configurator-step-two">
  <form id="form-topping-options">
    <div class="row row-configurator-picture">
      <div class="col-lg-8 mx-auto p-0">
        <div class="zone-wrap" data-zone-wrap="semmelteig">
          <img src="../app/themes/mypartybrezn-child/assets/images/configurator/semmelteig_mypartybrezn.jpg"
            alt="Partybrezn konfigurieren" title="Partybrezn konfigurieren" />
          <div class="zone-overlay">
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/semmelteig_zone1.png"
              class="overlay-zone" data-overlay-zone="zone-1" />
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/semmelteig_zone2.png"
              class="overlay-zone active" data-overlay-zone="zone-2" />
          </div>
        </div>
        <div class="zone-wrap" data-zone-wrap="3-kornteig">
          <img src="../app/themes/mypartybrezn-child/assets/images/configurator/dreikornteig_mypartybrezn.jpg"
            alt="Partybrezn konfigurieren" title="Partybrezn konfigurieren" />
          <div class="zone-overlay">
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/dreikornteig_zone1.png"
              class="overlay-zone" data-overlay-zone="zone-1" />
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/dreikornteig_zone2.png"
              class="overlay-zone overlay-zone-two active" data-overlay-zone="zone-2" />
          </div>
        </div>
        <div class="zone-wrap" data-zone-wrap="roggenteig">
          <img src="../app/themes/mypartybrezn-child/assets/images/configurator/roggenteig_mypartybrezn.jpg"
            alt="Partybrezn konfigurieren" title="Partybrezn konfigurieren" />
          <div class="zone-overlay">
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/roggenteig_zone1.png"
              class="overlay-zone overlay-zone-one" data-overlay-zone="zone-1" />
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/roggenteig_zone2.png"
              class="overlay-zone overlay-zone-two active" data-overlay-zone="zone-2" />
          </div>
        </div>
        <div class="zone-wrap" data-zone-wrap="formbrezn">
          <img src="../app/themes/mypartybrezn-child/assets/images/configurator/formbrezn.jpg"
            alt="Partybrezn konfigurieren" title="Partybrezn konfigurieren" />
          <div class="zone-overlay">
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/formbrezn_zone1.png"
              class="overlay-zone overlay-zone-one" data-overlay-zone="zone-1" />
            <img src="../app/themes/mypartybrezn-child/assets/images/configurator/formbrezn_zone2.png"
              class="overlay-zone overlay-zone-two active" data-overlay-zone="zone-2" />
          </div>
        </div>
      </div>
    </div>
    <div class="row row-zones">
      <div class="col-xs-8 col-sm-6 col-md-4 col-lg-2 mx-auto pb-3">
        <div class="zone-selection">
          <span class="zone-selector active" data-zone-selector="zone-1">Zone 1</span>
          <span class="zone-selector" data-zone-selector="zone-2">Zone 2</span>
        </div>
      </div>
    </div>

    <div class="row row-recommendations">
      <div class="col-xs-10 col-sm-8 mx-auto pb-5">
        <div class="recommendations">
          <p class="text-information">Auf Basis der angegebenen Personenanzahl empfehlen wir einen Belag von mindestens
            <span class="recommendation-amount">800</span> Gramm.</p>
        </div>
      </div>
    </div>

    <?php
    for ($zone = 1; $zone <= 2; $zone++) {
        ?>

    <div class="row row-ingredients <?php if ($zone == 1) {
            echo 'active';
        } ?>" data-zone="ingredients-zone-<?php echo $zone ?>">

      <?php
      $i = 0;

      foreach ($ingredients as $ingredient_group => $value) {
      ?>

      <div class="col-6 col-sm-4 col-lg-3">
        <img
          src="../app/themes/mypartybrezn-child/assets/images/configurator/zutaten_<?php echo strtolower(str_replace('ä', 'ae', $ingredient_group)); ?>.jpg"
          class="configurator-thumbnail" />
        <span
          class="toggle-submenu toggle-<?php echo $ingredient_group; if ($i == 0) echo ' submenu-opened'; ?>"><?php echo $ingredient_group; ?></span>
        <div class="submenu submenu-<?php echo $ingredient_group; if ($i == 0) echo ' active'; ?>">
          <ul>

            <?php
        foreach ($ingredients[$ingredient_group] as $ingredient => $value) {
          $checked = ($ingredient == 0) ? 'checked' : ''; 

          if ($ingredient_group == 'Aufstrich') { ?>

            <li>
              <input type="radio" name="aufstrich-zone<?php echo $zone ?>" value="<?php echo $value->name ?>"
                <?php echo $checked ?> />
              <label
                for="<?php echo $ingredient_group; ?>-<?php echo $value->name . '-zone' . $zone ?>"><?php echo $value->name ?></label>
              <input type="hidden" data-id="<?php echo $value->id ?>" />
              <input type="hidden" data-price="<?php echo $value->price ?>" />
              <input type="hidden" data-price-secondary="<?php echo $value->price_secondary ?>" />
            </li>

            <?php } 
          
            else if ($ingredient_group == 'Garnierung') { ?>

            <li>
              <input type="checkbox"
                name="<?php echo $ingredient_group; ?>-<?php echo str_replace(' ', '-', $value->name) . '-zone' . $zone ?>"
                value="<?php echo $value->name ?>" />
              <label
                for="<?php echo $ingredient_group; ?>-<?php echo $value->name . '-zone' . $zone ?>"><?php echo $value->name ?></label>
              <input type="hidden" data-id="<?php echo $value->id ?>" />
              <input type="hidden" data-price="<?php echo $value->price ?>" />
              <input type="hidden" data-price-secondary="<?php echo $value->price_secondary ?>" />
            </li>

            <?php } 
            
            else { ?>

            <li>
              <input type="number" step="100" min="0" max="1500"
                name="<?php echo $ingredient_group; ?>-<?php echo str_replace(' ', '-', $value->name) . '-zone' . $zone ?>"
                value="0">
              <label
                for="<?php echo $ingredient_group; ?>-<?php echo $value->name . '-zone' . $zone ?>"><?php echo $value->name ?></label>
              <input type="hidden" data-id="<?php echo $value->id ?>" />
              <input type="hidden" data-price="<?php echo $value->price ?>" />
              <input type="hidden" data-price-secondary="<?php echo $value->price_secondary ?>" />
            </li>

            <?php } 
      } ?>

          </ul>
        </div>
      </div>

      <?php

        $i++;
      }
      ?>

    </div>

    <?php
    } ?>

    <div class="row justify-content-md-center">
      <p class="configurator-price pt-4 mb-0">
        <span class="woocommerce-Price-amount amount">
          <span class="woocommerce-Price-currencySymbol">€ </span>
          <span class="final-price"><?php echo $_SESSION['initial_price'] ?></span>
        </span>
      </p>
    </div>
    <div class="row justify-content-md-center pt-4">
      <p class="mb-0">
        <span class="error-text"></span>
      </p>
    </div>
  </form>
</div>